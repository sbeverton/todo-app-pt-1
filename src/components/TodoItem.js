import React from "react";

const TodoItem = (props) => {
  return (
    <li className={props.completed ? "completed" : ""}>
      <div className="view">
        <input
          className="toggle"
          onChange={props.toggleComplete}
          type="checkbox"
          checked={props.completed}
        />
        <label>{props.title}</label>
        <button onClick={props.handleDelete} className="destroy" />
      </div>
    </li>
  );
};

export default TodoItem;
