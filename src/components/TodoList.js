import React from "react";
import TodoItem from "./TodoItem";

const TodoList = (props) => {
  return (
    <section className="main">
      <ul className="todo-list">
        {props.todos.map((todo) => (
          <TodoItem
            title={todo.title}
            key={todo.id}
            id={todo.id}
            completed={todo.completed}
            toggleComplete={(e) => props.toggleComplete(todo.id)}
            handleDelete={(e) => props.handleDelete(todo.id)}
          />
        ))}
      </ul>
    </section>
  );
};

export default TodoList;
