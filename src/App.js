import React, { Component } from "react";
import TodoList from "./components/TodoList";
import "./App.css";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todos: [],
      newTodoTitle: "",
    };
  }

  handleChange = (event) => {
    this.setState({
      newTodoTitle: event.target.value,
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    if (!this.state.newTodoTitle) {
      return;
    }
    const newTodo = {
      title: this.state.newTodoTitle,
      completed: false,
      key: Math.random() + 100,
      id: Math.random() + 200,
    };
    this.setState({ todos: [...this.state.todos, newTodo], newTodoTitle: "" });
  };

  handleClear = () => {
    const incompleteTodos = this.state.todos.filter(
      (todo) => todo.completed === false
    );
    this.setState({
      todos: incompleteTodos,
    });
  };

  handleDelete = (id) => {
    const updatedTodos = this.state.todos.filter((todo) => todo.id !== id);
    this.setState({
      todos: updatedTodos,
    });
  };

  toggleComplete = (id) => {
    let updatedTodos = this.state.todos.map((todo) => {
      if (todo.id === id) {
        return {
          ...todo,
          completed: !todo.completed,
        };
      }
      return todo;
    });
    this.setState({ todos: updatedTodos });
  };

  render() {
    const incompleteTodos = this.state.todos.filter(
      (todo) => todo.completed === false
    ).length;
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <form onSubmit={this.handleSubmit}>
            <input
              onChange={this.handleChange}
              value={this.state.newTodoTitle}
              className="new-todo"
              placeholder="What needs to be done?"
              autoFocus
            />
          </form>
        </header>
        <TodoList
          todos={this.state.todos}
          handleDelete={this.handleDelete}
          toggleComplete={this.toggleComplete}
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>{incompleteTodos}</strong> item(s) left
          </span>
          <button onClick={this.handleClear} className="clear-completed">
            Clear completed
          </button>
        </footer>
      </section>
    );
  }
}

export default App;
